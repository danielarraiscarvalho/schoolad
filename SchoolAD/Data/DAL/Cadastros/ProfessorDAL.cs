﻿using Microsoft.EntityFrameworkCore;
using Modelo.Docente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolAD.Data.DAL.Cadastros
{
    public class ProfessorDAL
    {
        private Context _context;

        public ProfessorDAL(Context context) {
            _context = context;
        }

        public IQueryable<Professor> ObterProfessoresClassificadosPorNome() {
            return _context.Professores.OrderBy(b => b.Nome);
        }

        public async Task<Professor> ObterProfessorPorId(long id) {
            var professor = await _context.Professores.SingleOrDefaultAsync(m => m.ProfessorID == id);
            return professor;
        }

        public async Task<Professor> EliminarProfessorPorId(long id) {
            Professor professor = await ObterProfessorPorId(id);
            _context.Professores.Remove(professor);
            await _context.SaveChangesAsync();
            return professor;
        }

        public async Task<Professor> GravarProfessor(Professor professor) {
            if (professor.ProfessorID == null) {
                _context.Professores.Add(professor);
            } else {
                _context.Update(professor);
            }
            await _context.SaveChangesAsync();
            return professor;
        }
    }
}
