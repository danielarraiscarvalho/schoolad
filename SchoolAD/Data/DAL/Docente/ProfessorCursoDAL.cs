﻿using Microsoft.EntityFrameworkCore;
using Modelo.Docente;
using System.Linq;

namespace SchoolAD.Data.DAL.Docente
{
    public class ProfessorCursoDAL
    {
        private Context _context;

        public ProfessorCursoDAL(Context context)
        {
            _context = context;
        }

        public IQueryable<Professor> ObterProfessoresClassificadosPorNome()
        {
            return _context.Professores.OrderBy(b => b.Nome);
        }
    }
}
