﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Modelo.Docente;
using SchoolAD.Data;
using SchoolAD.Data.DAL.Cadastros;

namespace SchoolAD.Areas.Cadastros.Controllers {

    [Area("Cadastros")]
    [Authorize]
    public class ProfessorController : Controller {
        private readonly Context _context;
        private readonly Data.DAL.Cadastros.ProfessorDAL cursoDAL;

        public ProfessorController(Context context) {
            _context = context;
            this.cursoDAL = new ProfessorDAL(context);
        }

        public IActionResult Index() {
            return View(cursoDAL.ObterProfessoresClassificadosPorNome());
        }

        private async Task<IActionResult> ObterVisaoProfessorPorId(long? id) {
            if (id == null) {
                return NotFound();
            }

            var curso = await cursoDAL.ObterProfessorPorId((long)id);
            if (curso == null) {
                return NotFound();
            }

            return View(curso);
        }

        public async Task<IActionResult> Details(long? id) {
            return await ObterVisaoProfessorPorId(id);
        }

        public async Task<IActionResult> Edit(long? id) {
            return await ObterVisaoProfessorPorId(id);
        }

        public async Task<IActionResult> Delete(long? id) {
            return await ObterVisaoProfessorPorId(id);
        }

        // GET: Professor/Create
        public IActionResult Create() {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nome")] Professor curso) {
            try {
                if (ModelState.IsValid) {
                    await cursoDAL.GravarProfessor(curso);
                    return RedirectToAction(nameof(Index));
                }
            } catch (DbUpdateException) {
                ModelState.AddModelError("", "Não foi possível inserir os dados.");
            }
            return View(curso);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long? id, [Bind("ProfessorID,Nome")] Professor Professor) {
            if (id != Professor.ProfessorID) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                try {
                    await cursoDAL.GravarProfessor(Professor);
                } catch (DbUpdateConcurrencyException) {
                    if (!await ProfessorExists(Professor.ProfessorID)) {
                        return NotFound();
                    } else {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(Professor);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long? id) {
            var Professor = await cursoDAL.EliminarProfessorPorId((long)id);
            TempData["Message"] = "Professor " + Professor.Nome.ToUpper() + " foi removido";
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> ProfessorExists(long? id) {
            return await cursoDAL.ObterProfessorPorId((long)id) != null;

        }
    }
}