﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Modelo.Cadastros;
using SchoolAD.Data;
using SchoolAD.Data.DAL.Cadastros;

namespace SchoolAD.Areas.Cadastros.Controllers
{
    [Area("Cadastros")]
    [Authorize]
    public class CursoController : Controller
    {

        private readonly Context _context;
        private readonly Data.DAL.Cadastros.CursoDAL cursoDAL;

        public CursoController(Context context) {
            _context = context;
            this.cursoDAL = new CursoDAL(context);
        }

        public IActionResult Index() {
            return View(cursoDAL.ObterCursosClassificadosPorNome());
        }

        private async Task<IActionResult> ObterVisaoCursoPorId(long? id) {
            if (id == null) {
                return NotFound();
            }

            var curso = await cursoDAL.ObterCursoPorId((long)id);
            if (curso == null) {
                return NotFound();
            }

            return View(curso);
        }

        public async Task<IActionResult> Details(long? id) {
            return await ObterVisaoCursoPorId(id);
        }

        public async Task<IActionResult> Edit(long? id) {
            PrepareViewBag(new DepartamentoDAL(_context).ObterDepartamentosClassificadosPorNome().ToList(), new InstituicaoDAL(_context).ObterInstituicoesClassificadasPorNome().ToList());
            return await ObterVisaoCursoPorId(id);
        }

        public async Task<IActionResult> Delete(long? id) {
            return await ObterVisaoCursoPorId(id);
        }

        public void PrepareViewBag(List<Departamento> departamentos, List<Instituicao> instituicoes) {
            departamentos.Insert(0, new Departamento() { DepartamentoID = 0, Nome = "Selecione o departamento" });
            instituicoes.Insert(0, new Instituicao() { InstituicaoID = 0, Nome = "Selecione a Instituicao" });
            ViewBag.Departamentos = departamentos;
            ViewBag.Instituicoes = instituicoes;
        }

        // GET: Curso/Create
        public IActionResult Create() {
            PrepareViewBag(new List<Departamento>(), new InstituicaoDAL(_context).ObterInstituicoesClassificadasPorNome().ToList());
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nome,DepartamentoID")] Curso curso) {
            try {
                if (ModelState.IsValid) {
                    await cursoDAL.GravarCurso(curso);
                    return RedirectToAction(nameof(Index));
                }
            } catch (DbUpdateException) {
                ModelState.AddModelError("", "Não foi possível inserir os dados.");
            }
            return View(curso);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long? id, [Bind("CursoID,Nome,DepartamentoID")] Curso Curso) {
            if (id != Curso.CursoID) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                try {
                    await cursoDAL.GravarCurso(Curso);
                } catch (DbUpdateConcurrencyException) {
                    if (!await CursoExists(Curso.CursoID)) {
                        return NotFound();
                    } else {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(Curso);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long? id) {
            var Curso = await cursoDAL.EliminarCursoPorId((long)id);
            TempData["Message"] = "Curso " + Curso.Nome.ToUpper() + " foi removido";
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> CursoExists(long? id) {
            return await cursoDAL.ObterCursoPorId((long)id) != null;
        }

        public JsonResult ObterDepartamentosPorInstituicao(long actionID) {
            var departamentos = new DepartamentoDAL(_context).ObterDepartamentoPorInstituicao(actionID).ToList();
            departamentos.Insert(0, new Departamento() { DepartamentoID = 0, Nome = "Selecione o departamento" });
            return Json(new Microsoft.AspNetCore.Mvc.Rendering.SelectList(departamentos, "DepartamentoID", "Nome"));
        }
    }
}